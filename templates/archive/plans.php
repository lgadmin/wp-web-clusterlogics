<?php
/**

 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content site-blog mb-sm">
			<main>
				<div class="container-flex mt-lg">
						<?php if ( have_posts() ) : ?>
							<?php $i = 0; ?>
							<?php while ( have_posts() ) : the_post(); ?>


								<div class="pt-lg pb-lg <?php if($i === 1){echo 'bg-gray-lighter';} ?> ">
									<div class="container">
										
						                <div class="panel panel-default">
						                  <div class="panel-heading"><h2><?php echo get_the_title(); ?></h2></div>
						                  <div class="panel-body"> <?php the_content(); ?> </div>
						                  <div class="panel-footer">
						                      <button type="button" class="btn btn-primary text-uppercase btn-block mt-lg" data-toggle="modal" data-target="#requestquote"> Order Now </button>
						                  </div>
						                </div>

									</div>
								</div>


							<?php endwhile; ?>
							<?php if ($i === 0) {$i++; } else{$i = 0; } ?>
						<?php endif ?>
				</div>
			</main>
		</div>
	</div>
	
<?php get_footer(); ?>