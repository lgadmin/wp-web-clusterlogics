<?php if( have_rows('cta_panel') ): ?>
<section id="custom-cta-services" class="container">
	<?php while ( have_rows('cta_panel') ) : the_row(); ?>
	
		<div class="myitem">
			<div class="panel panel-default text-center">
				<div class="panel-body">
					<h2><?php the_sub_field('cta_panel_title'); ?></h2>
					<?php echo wp_get_attachment_image(get_sub_field('cta_panel_icon'), 'full' ); ?>
					<a class="btn btn-primary text-uppercase btn-block mt-lg" href="<?php the_sub_field('cta_panel_link'); ?>"><?php the_sub_field('cta_panel_button_label'); ?></a>
				</div>
			</div>
		</div>

	<?php endwhile; ?>
</section>
<?php endif; ?>
		
				