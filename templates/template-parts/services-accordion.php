
<?php if( have_rows('services_accordion') ): ?>
 <div class="panel-group" id="technical-accordion" role="tablist" aria-multiselectable="true">

  <?php while ( have_rows('services_accordion') ) : the_row(); ?>
  
  <?php 
    $accordionID = get_sub_field('services_accordion_label');
    $accordionID = str_replace(" ", "-", $accordionID); 
    $accordionID = strtolower($accordionID);
  ?>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h2 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $accordionID; ?>" aria-expanded="false" aria-controls="<?php echo $accordionID; ?>">
            <?php the_sub_field('services_accordion_label'); ?>
          </a>
        </h2>
      </div>
      <div id="<?php echo $accordionID; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <?php the_sub_field('services_accordion_content'); ?>
        </div>
      </div>
    </div>
  <?php endwhile; ?>

</div>

<?php endif; ?>