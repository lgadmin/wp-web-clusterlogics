<?php 	
	$projects_query = array(
        'showposts' => -1,
        'post_type' => 'services',
    );

    $projects_query_results = new WP_Query( $projects_query );
?>

<div id="tct-services">
	<div class="body-copy">
		<div class="flex-container">
			<?php if ( $projects_query_results->have_posts() ) : ?>
				<?php while ( $projects_query_results->have_posts() ) :  $projects_query_results->the_post(); ?>

						<a href="<?php the_permalink(); ?>" class="thumbnail">
							<div class="img-cont text-center">
								<div class="circular-square"> <?php echo wp_get_attachment_image( get_field('service_icon'), 'full' ); ?> </div>
							</div>
							<div class="caption">
								<h2 class="mytitle"><?php echo get_the_title(); ?></h2>
								<?php if (! is_front_page()): ?>
									<?php echo get_the_excerpt(); ?>
									<div class="btn btn-link btn-block">Learn more</div>
								<?php endif ?>
							</div>
						</a>
					
				<?php endwhile; ?>
			<?php endif ?>			        
		</div>
	</div>
</div>

<?php wp_reset_query(); ?>