<?php if( have_rows('services_flex_cta') ):
	while ( have_rows('services_flex_cta') ) : the_row();
		
		switch ( get_row_layout()) {

			// Alpha
			case 'cta_alpha':
				get_template_part('/templates/template-parts/services-flexible/cta-alpha');
			break;
			
			// icon_block
			case 'icon_block':
				get_template_part('/templates/template-parts/services-flexible/cta-icon-block');
			break;

			default:
				echo "<!-- nothing to see here -->";
			break;
		}

	endwhile; else : // no layouts found 
endif; ?>