<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-sm">
	<div class="cta-alpha <?php the_sub_field('container'); ?>">
		<div class="cta-body">
			<div class="container-flex">
				<div class="cta-icon"><?php echo wp_get_attachment_image( get_sub_field('icon_block_icon'), 'full' ); ?></div>
				<div class="cta-icon-copy"><?php the_sub_field('icon_block_copy'); ?></div>
			</div>
		</div>
	</div>
</section>
