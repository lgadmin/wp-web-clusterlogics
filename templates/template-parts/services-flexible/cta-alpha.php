<section class="<?php the_sub_field('background_colour'); ?> clearfix pt-sm pb-sm">
	<div class="cta-alpha <?php the_sub_field('container'); ?>">
		<div class="cta-body">
			<?php the_sub_field('content_alpha'); ?>
		</div>
	</div>
</section>
