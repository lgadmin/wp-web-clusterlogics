<?php   
 
  // Plans Query
  $plans_query = array(
    'showposts' => -1,
    'post_type' => 'plans',
  );

  $plans_query_results = new WP_Query( $plans_query );


  // Categorie list
  $cat_args=array(
    'orderby' => 'name',
    'order'   => 'ASC',
    'taxonomy' => 'plans-category'
  );

  $mycat = get_categories($cat_args, 'plans');
  // echo '<pre>'; print_r($mycat); echo '</pre>';
?>


<div class="nav-cont">
  <!-- Nav tabs -->
  <ul class="nav nav-pills mb-lg" role="tablist">
    <?php $i = 0; $len = count($mycat); ?>
    <?php foreach($mycat as $dcat) : ?>
        <li role="presentation" <?php if($i === 0){echo 'class="active"';} ?>><a href="#<?php echo $dcat->slug; ?>" onclick="javascript:void(0)" role="tab" data-toggle="tab"><?php echo $dcat->name; ?></a></li>  
      <?php  $i++; ?> 
    <?php endforeach; ?>
  </ul>
</div>

  <!-- Tab panes -->
  <div class="tab-content">
    <?php $c = 0; $len = count($mycat); ?>
    <?php foreach($mycat as $tcat) : ?>
      <div role="tabpanel" class="tab-pane <?php if($c === 0){echo 'active';} ?>" id="<?php echo $tcat->slug; ?>">
       
          <?php if ( $plans_query_results->have_posts() ) : ?>
            <?php while ( $plans_query_results->have_posts() ) :  $plans_query_results->the_post(); ?>
              <?php if ( has_term( $tcat->name, 'plans-category' )) : ?>
            
                <div class="panel panel-default">
                  <div class="panel-heading"><h2><?php echo get_the_title(); ?></h2></div>
                  <div class="panel-body"> <?php the_content(); ?> </div>
                  <div class="panel-footer">
                      <a href="<?php the_field('order_now_link'); ?>" type="button" class="btn btn-primary text-uppercase btn-block mt-lg" target="_blank"><?php the_field('order_now_label'); ?></a>
                  </div>
                </div>
              
              <?php endif; ?>
            <?php endwhile; ?>
          <?php endif ?>
          <?php wp_reset_query(); ?>  

      </div>
      <?php  $c++; ?>
    <?php endforeach; ?>
  </div>
  

