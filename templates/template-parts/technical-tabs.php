<div id="technical-accordion">
  <div class="nav-cont">
    <!-- Nav tabs -->
    <ul class="nav nav-pills mb-lg" role="tablist">
        <li role="presentation" class="active" >
          <a href="#hosted-plans" role="tab" data-toggle="tab"> HOSTED PLANS </a>
          <?php echo wp_get_attachment_image(get_field('hosted_technical_icon'), 'full'); ?> 
        </li>  
        <li role="presentation">
          <a href="#self-hosted-plans" role="tab" data-toggle="tab"> SELF HOSTED PLANS </a>
          <?php echo wp_get_attachment_image(get_field('self_hosted_technical_icon'), 'full'); ?>
        </li>  
    </ul>


  <!-- Tab panes -->
  <div class="tab-content">
    
    <div role="tabpanel" class="tab-pane active" id="hosted-plans">
        <?php get_template_part( '/templates/template-parts/technical-accordion' ); ?> 
    </div>
    
    <div role="tabpanel" class="tab-pane" id="self-hosted-plans">
        <?php get_template_part( '/templates/template-parts/technical-accordion', 'self' ); ?> 
    </div>

  </div>


  </div>
</div>