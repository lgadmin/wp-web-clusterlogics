
<?php if( have_rows('hosted_technical_tabs') ): ?>
<?php $i = 0; ?>
 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

  <?php while ( have_rows('hosted_technical_tabs') ) : the_row(); ?>
  
  <?php 
    $accordionID = get_sub_field('hosted_technical_tab_label');
    $accordionID = str_replace(" ", "-", $accordionID); 
    $accordionID = strtolower($accordionID);
  ?>

    <div class="panel panel-default">
      <div class="panel-heading" role="tab" id="headingOne">
        <h2 class="panel-title">
          <a role="button" data-toggle="collapse" data-parent="#accordion" href="#<?php echo $accordionID; ?>" aria-expanded="<?php if ($i == 0) { echo 'true'; } ?>" aria-controls="<?php echo $accordionID; ?>">
            <?php the_sub_field('hosted_technical_tab_label'); ?>
          </a>
        </h2>
      </div>
      <div id="<?php echo $accordionID; ?>" class="panel-collapse collapse <?php if ($i == 0) { echo 'in'; } ?>" role="tabpanel" aria-labelledby="headingOne">
        <div class="panel-body">
          <?php the_sub_field('hosted_technical_tab_content'); ?>
        </div>
      </div>
    </div>
    <?php $i++; ?>
  <?php endwhile; ?>

</div>

<?php endif; ?>