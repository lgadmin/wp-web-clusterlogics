<!-- Modal -->
<div class="modal fade" id="requestquote" tabindex="-1" role="dialog" aria-labelledby="requestquoteLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <?php echo do_shortcode( '[gravityform id="1" title="true" description="true"]' ) ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>