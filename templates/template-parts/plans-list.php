<?php 	

	if (is_page( 'hosted-plans' )) {
		$mycat = 'hosted-plans';
	} elseif (is_page( 'self-hosted-plans' )) {
		$mycat = 'self-hosted-plans';
	} else {
		$mycat = 'self-hosted-plans, hosted-plans';
	}


	$plans_query = array(
        'post_type'     => 'plans',
           'tax_query'   => [
            [
                'taxonomy' => 'plans-category',
                'field'    => 'slug',
                'terms'    => $mycat
            ]
        ]
    );

    $plans_query_results = new WP_Query( $plans_query );

?>


<?php if ( $plans_query_results->have_posts() ) : ?>
	<?php $i = 0; ?>
	<?php while ( $plans_query_results->have_posts() ) :  $plans_query_results->the_post(); ?>
		<div class="pt-lg pb-lg <?php if($i === 1){echo 'bg-gray-lighter';} ?> ">
			<div class="container">
				
                <div class="panel panel-default">
                  <div class="panel-heading"><h2><?php echo get_the_title(); ?></h2></div>
                  <div class="panel-body"> <?php the_content(); ?> </div>
                  <div class="panel-footer">
                      <button type="button" class="btn btn-primary text-uppercase btn-block mt-lg" data-toggle="modal" data-target="#requestquote"> Order Now </button>
                  </div>
                </div>

			</div>
		</div>
	<?php if ($i === 0) {$i++; } else{$i = 0; } ?>
	<?php endwhile; ?>
<?php endif ?>

<?php wp_reset_query(); ?>