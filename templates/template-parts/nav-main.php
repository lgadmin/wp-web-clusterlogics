<div class="main-navigation">
  	<nav class="navbar navbar-default">  
  	   
        <!-- Brand and toggle get grouped for better mobile display -->
  	    <div class="navbar-header">
  	      
          <div class="navbar-toggle-cont"> 
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar">
    	        <span class="sr-only">Toggle navigation</span>
              <i class="fa fa-bars" aria-hidden="true"></i>
              Menu
    	      </button>
          </div>
  	    
          <div class="navbar-header-logo">
            <?php
              if(shortcode_exists('lg-site-logo')){
                echo do_shortcode('[lg-site-logo]');
              }else{
                the_custom_logo();
              }
            ?>
          </div>


        </div>
       

        <!-- Main Menu  -->
        <?php 

          $mainMenu = array(
          	// 'menu'              => 'menu-1',
          	// 'theme_location'    => 'top-nav',
          	'depth'             => 2,
          	'container'         => 'div',
          	'container_class'   => 'collapse navbar-collapse',
          	'container_id'      => 'main-navbar',
          	'menu_class'        => 'nav navbar-nav navbar-right',
          	'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
          		// 'walker'         => new WP_Bootstrap_Navwalker_Custom()  // Custom used in Skin Method.
          	'walker'            => new WP_Bootstrap_Navwalker()
          );
          wp_nav_menu($mainMenu);

        ?>
  	</nav>
</div>