<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content mb-sm">
			<main>
				<div class="container-flex mt-lg">
					<div class="content">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<!-- Main Loop -->
								<h1 class="text-center mb-lg"><?php the_title(); ?></h1>
								<?php the_content(); ?>
							<?php endwhile; ?>
						<?php endif ?>
					</div>
					<?php // get_sidebar(); ?>
				</div>

				<?php get_template_part( '/templates/template-parts/services-flexible/cta-flexible' ); ?>

				<div class="container clearfix mt-lg">
					<?php get_template_part( '/templates/template-parts/services-accordion' ); ?>	
				</div>

				<?php get_template_part( '/templates/template-parts/custom-cta-services' ); ?>

			</main>
		</div>
	</div>
	
<?php get_footer(); ?>