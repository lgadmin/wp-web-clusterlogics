<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				
				<div class="container mb-lg">
					<div class="body-copy">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
							<?php endwhile; ?>
						<?php endif ?>
					</div>
				</div>






<?php 	
	$services_query = array(
        'showposts' => -1,
        'post_type' => 'services',
    );

    $services_query_results = new WP_Query( $services_query );
?>


<?php if ( $services_query_results->have_posts() ) : ?>
	<?php $i = 0; ?>
	<?php while ( $services_query_results->have_posts() ) :  $services_query_results->the_post(); ?>
		<div class="pt-lg pb-lg <?php if($i === 1){echo 'bg-gray-lighter';} ?> ">
			<div class="container">
				
				<section class="thumbnail main-services">
					
					<div class="img-cont text-center">
						<div class="circular-square">
							<a href="<?php the_permalink(); ?>"><?php echo wp_get_attachment_image( get_field('service_icon'), 'full' ); ?></a>
						</div>
					</div>
					
					<div class="caption">
						<h2><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h2>
						<?php the_field('service_excerpt'); ?>
					</div>

					<div class="content">
						<?php the_field('service_cta_copy'); ?>
					</div>

				</section>

			</div>
		</div>
	<?php if ($i === 0) {$i++; } else{$i = 0; } ?>
	<?php endwhile; ?>
<?php endif ?>			        


<?php wp_reset_query(); ?>				

			</main>
		</div>
	</div>

<?php get_footer(); ?>