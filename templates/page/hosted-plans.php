<?php
/**

 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content">
			<main>
				
				<div class="container mb-lg">
					<div class="body-copy">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<?php the_content(); ?>
		                      <button type="button" class="btn btn-primary btn-lg text-uppercase mt-lg center-block" data-toggle="modal" data-target="#requestquote"> Order Now </button>
							<?php endwhile; ?>
						<?php endif ?>
					</div>
				</div>		

				<?php get_template_part( '/templates/template-parts/plans-list' ); ?>	

			</main>
		</div>
	</div>
	
<?php get_footer(); ?>