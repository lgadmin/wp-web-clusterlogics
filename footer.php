<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */

?>

	<footer id="colophon" class="site-footer">
		<div id="site-footer" class="bg-gray-light clearfix pb-lg pt-lg">
			
			<section class="site-footer-logo small">
				<?php
					if(shortcode_exists('lg-site-logo')){
						echo do_shortcode('[lg-site-logo]');
					}else{
						the_custom_logo();
					}
			 	?>
				<?php echo get_field('footer_blurb', 'option'); ?>
		 	</section>
			
			<section class="site-footer-form">
				<?php echo do_shortcode('[gravityform id="1" title="true" description="false"]'); ?>
			</section>
			
			<section class="site-footer-utility">
				<h2>Contact Us</h2>
				<ul class="list-unstyled mb-lg">
					<li><a href="mailto:+1<?php echo do_shortcode( '[lg-phone-alt]' ); ?>">1-<?php echo format_phone(do_shortcode('[lg-phone-alt]')); ?></a></li>
					<li><a href="mailto:<?php echo do_shortcode( '[lg-email]' ); ?>"><?php echo do_shortcode( '[lg-email]' ); ?></a></li>
				</ul>
								
				<h2>Stay Connected</h2>
				<?php echo do_shortcode('[lg-social-media]'); ?>
			</section>

		</div>

		<div id="site-legal" class="bg-gray-dark pt-sm pb-sm clearfix">
			<div class="site-info"><?php get_template_part("/templates/template-parts/site-info"); ?></div>
			<div class="site-longevity"> <?php get_template_part("/templates/template-parts/site-footer-longevity"); ?> </div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php get_template_part( '/templates/template-parts/modal-contact' ) ?>

<?php wp_footer(); ?>

</body>
</html>
