<?php
/**
 * Main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Eleven
 */

get_header('home'); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content site-page">
			<main>
				
				
				<div class="container clearfix mt-lg">
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>
							<?php the_content(); ?>
						<?php endwhile; ?>
					<?php endif ?>
				</div>

				<section id="our-services" class="container clearfix mb-lg">
					<div class="text-center pb-sm">
					<?php the_field('services_copy'); ?>
					</div>
					<?php get_template_part( '/templates/template-parts/service-grid' ); ?>					
				</section>

				<div id="our-plans" class="bg-gray-lighter pt-lg pb-lg">
					<section class="container">
						<?php the_field('plans_copy'); ?>
						<?php get_template_part( '/templates/template-parts/plans-nav-tab' ); ?>	
					</section>
				</div>


			</main>
		</div>
	</div>

<?php get_footer(); ?>