<?php
/**
 * The sidebar containing the main widget area
 *
 */

if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area">
	<?php 
		if ('supports' == get_post_type()) {
			dynamic_sidebar( 'support-1' );
		} else{
			dynamic_sidebar( 'sidebar-1' );
		}
	?>
</aside>
