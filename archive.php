<?php
/**

 */

get_header(); ?>

	<div id="primary">
		<div id="content" role="main" class="site-content site-blog mb-sm">
			<main>
				<div class="container-flex mt-lg">
					<div class="content">
						<?php if ( have_posts() ) : ?>
							<?php while ( have_posts() ) : the_post(); ?>
								<!-- Main Loop -->
								<h1><?php the_title(); ?></h1>
								<hr>
								<p><?php the_content(); ?></p>
							<?php endwhile; ?>
						<?php endif ?>
					</div>
					<?php get_sidebar(); ?>
				</div>
			</main>
		</div>
	</div>
	
<?php get_footer(); ?>