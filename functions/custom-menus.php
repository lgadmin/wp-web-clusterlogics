<?php

function create_menu() {
    
    add_menu_page(
        __( 'Cluster Logics', 'Cluster Logics' ),
        'Cluster Logics',   
        'manage_options',
        'cluster_logics',
        '',
        'dashicons-admin-site',
        2
    );

}

add_action( 'admin_menu', 'create_menu' );

?>