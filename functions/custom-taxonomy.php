<?php

function create_taxonomy(){

	// Supports
	register_taxonomy(
		'supports-category',
		'supports',
		array(
			'label'        => __( 'Supports Category' ),
			'rewrite'      => array( 'slug' => 'supports-category' ),
			'hierarchical' => true,
		)
	);

	// Plans
	register_taxonomy(
		'plans-category',
		'plans',
		array(
			'label'        => __( 'Plans Category' ),
			'rewrite'      => array( 'slug'     => 'plans-category' ),
			'hierarchical' => true,
		)
	);
}

add_action( 'init', 'create_taxonomy' );

?>