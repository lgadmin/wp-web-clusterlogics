<?php

//Custom Post Types
function create_post_type() {

    // Services
    $services_labels = array(
      'name'          => __( 'Services' ),
      'singular_name' => __( 'Service' ),
    );

    register_post_type( 'services',
        array(
          'labels'       => $services_labels,
          'public'       => true,
          'has_archive'  => false,
          'menu_icon'    => 'dashicons-location',
          'show_in_menu' => 'cluster_logics',
          'supports'     => array( 'thumbnail','title', 'editor', 'excerpt' ),
          'taxonomies'   => array('category', 'post_tag'),
        )
    );
    
    // Plans
    $plans_labels = array(
      'name'          => __( 'Plans' ),
      'singular_name' => __( 'Plan' ),
    );

    register_post_type( 'plans',
        array(
          'labels'       => $plans_labels,
          'public'       => true,
          'has_archive'  => true,
          'menu_icon'    => 'dashicons-location',
          'show_in_menu' => 'cluster_logics',
          'supports'     => array( 'thumbnail','title', 'editor', 'excerpt' ),
          'rewrite' => array('slug' => 'our-plans','with_front' => false),
          // 'taxonomies'   => array('category', 'post_tag'),
        )
    );

    // Open Sources
    $opensources_labels = array(
      'name'          => __( 'Open Sources' ),
      'singular_name' => __( 'Open Source' ),
    );

    register_post_type( 'opensources',
        array(
          'labels'       => $opensources_labels,
          'public'       => true,
          'has_archive'  => false,
          'menu_icon'    => 'dashicons-location',
          'show_in_menu' => 'cluster_logics',
          'supports'     => array( 'thumbnail','title', 'editor', 'excerpt' ),
          'taxonomies'   => array('category', 'post_tag'),
        )
    );

}
add_action( 'init', 'create_post_type' );

?>