<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package _s
 */
 
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="TYWOJ9l3PKfDHw0088S4LsEPEynBeiqIioeNVS54XqE" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<meta name="google-site-verification" content="TYWOJ9l3PKfDHw0088S4LsEPEynBeiqIioeNVS54XqE" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div id="page" class="site">

	<header id="masthead" class="site-header">
		
		<div class="site-branding site-header-alpha">
			<div class="logo">
				<?php
					if(shortcode_exists('lg-site-logo')){
						echo do_shortcode('[lg-site-logo]');
					}else{
						the_custom_logo();
					}
			 	?>
			</div>
		</div><!-- .site-branding -->

    <section class="site-header-bravo bg-alpha">
        
        <div class="site-header-utility">
          <ul class="list-inline">
            <li><a href="mailto:+1<?php echo do_shortcode( '[lg-phone-alt]' ); ?>">1-<?php echo format_phone(do_shortcode('[lg-phone-alt]')); ?></a></li>
            <li><a href="mailto:<?php echo do_shortcode( '[lg-email]' ); ?>"><?php echo do_shortcode( '[lg-email]' ); ?></a></li>
          </ul>
        </div>

        <?php get_template_part( 'templates/template-parts/nav-main' ) ?>
      
        <section class="nav-utility">
          <ul class="list-inline">
            <li><a href="#" class="btn btn-default text-uppercase" data-toggle="modal" data-target="#requestquote"><span class="hidden-xs hidden-md">Request a </span>quote</a></li>
            <li><a target="_blank" href="<?php the_field('nav_utility_login_link', 'option'); ?>" class="btn btn-primary text-uppercase"><i class="fa fa-user" aria-hidden="true"></i>&nbsp; <?php the_field('nav_utility_login_label', 'option'); ?></a></li>
          </ul>
        </section>

    </section>

  </header><!-- #masthead -->
